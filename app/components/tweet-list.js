import Ember from 'ember';

export default Ember.Component.extend({
  tweets: [],
  actions: {
    buildList() {
      var self = this;

      this.$('#pagination-container').pagination({
        dataSource: self.get('tweets'),
        pageSize: 10,
        callback: function(data, pagination) {
          var html = '<ul class="list-group">';
          for (var i = 0; i < data.length; i++) {
            console.log(data[i]);
            html += '<li class="list-group-item">' + data[i].text + '<br><small>' + data[i].date + '</small></li>';
          }
          html += '</ul>';

          $('#data-container').html(html);
        }
      });
    },
    sendFormTweets() {
      var self = this;
      var tweet = this.get('tweet');
      var searchTerm = tweet;
      var numOfTweets = 20;
      var url = 'http://aamirafridi.com/twitter/?q=' + searchTerm + '&count=' + numOfTweets;

      return Em.$.ajax(url, {
        type: 'GET',
        dataType: "jsonp",
        success: function(data, textStatus, jqXHR) {
          if (self.get('tweets').length > 0) {
            self.get('tweets').clear();
          }
          $.each(data.statuses, function() {
            self.get('tweets').pushObject({
              'text': this.text,
              'date': this.created_at
            });
          });
          self.send('buildList');
          // return data;
        },
        error: function(jqXHR, textStatus, errorThrown) {
          window.console.log(jqXHR);
        }
      });
    },
    preventEnter() {
      this.$().keypress(function(event) {
        if (event.keyCode == 13) {
          event.preventDefault();
        }
        // this.send('actionBar');
      });
    }
  }
});
